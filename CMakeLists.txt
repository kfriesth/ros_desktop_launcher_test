cmake_minimum_required(VERSION 3.5.0)
project(ros_desktop_launcher_test)
#add_compile_options(-std=c++11 -Wall -Wextra) # We do not compile anything in this example

find_package(catkin REQUIRED COMPONENTS
  roscpp
)

###################################
## catkin specific configuration ##
###################################

catkin_package(
#  INCLUDE_DIRS
#  LIBRARIES 
  CATKIN_DEPENDS
  roscpp
)

###########
## Build ##
###########

# Empty

#############
## Install ##
#############

# Create directory for the Ubuntu desktop file/icon
# This only works on Ubuntu and for the current user
set(HOME_DIR $ENV{HOME})
set(UBUNTU_DESKTOP_APP_DIR "${HOME_DIR}/.local/share/applications")
install(DIRECTORY DESTINATION ${UBUNTU_DESKTOP_APP_DIR})

# Configure / install desktop launcher
configure_file(desktop/green_robot.desktop.in ${UBUNTU_DESKTOP_APP_DIR}/green_robot.desktop)
configure_file(desktop/red_robot.desktop.in ${UBUNTU_DESKTOP_APP_DIR}/red_robot.desktop)

# Install icon
install(FILES
  desktop/ros_dekstop_launcher_test.png
  DESTINATION ${UBUNTU_DESKTOP_APP_DIR}
)

#install(TARGETS
  # Your build targets here
#  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#  RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION}
#)

## Mark cpp header files for installation
#install(DIRECTORY include/${PROJECT_NAME}/
#  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
#)

## Mark other files for installation (e.g. launch and bag files, etc.)
INSTALL(DIRECTORY config DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})
INSTALL(DIRECTORY launch DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})
INSTALL(DIRECTORY urdf DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION})

#############
## Testing ##
#############

# Empty

