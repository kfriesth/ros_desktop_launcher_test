 [![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr) ROS desktop launcher test
===

[![build status](https://gitlab.com/InstitutMaupertuis/ros_desktop_launcher_test/badges/kinetic/build.svg)](https://gitlab.com/InstitutMaupertuis/ros_desktop_launcher_test/commits/kinetic)

This repository contains a [Robot Operating System](http://www.ros.org/) package to test ROS desktop launcher files.

<img src="/documentation/dash.png" align="center" height="200">

How to test
---
- Create a catkin work-space
- Clone this repository
```
git clone https://gitlab.com/InstitutMaupertuis/ros_desktop_launcher_test.git
```

- Install the work-space (`catkin_make` or `catkin tools`)
```
catkin_make install
catkin config --install && catkin build
```

You will notice that some files are installed in `$HOME/.local/share/applications/`.
Use `gtk-launch green_robot.desktop` and `gtk-launch red_robot.desktop` to launch the applications.

The dash usually takes a little bit of time (try logging-out/logging-in again) to update the available applications so it may not appear directly after install.

How it works
---
`.desktop` and `.png` files are installed in `$HOME/.local/share/applications/`, these files are parsed by Ubuntu and create applications.

The image file is the icon of the application displayed in the dash.

These files are automatically installed via CMake when install is required.

Here are the steps in the CMake script:
- [Make sure the `$HOME/.local/share/applications/` exists (create it if necessary)](/CMakeLists.txt#L31-35)
- [Configure the `desktop.in` files in the `applications` directory](/CMakeLists.txt#L36-38): CMake variables like `${PROJECT_NAME}` are replaced by CMake and the file is created in the target directory
- [Copy application icon file](/CMakeLists.txt#L40-44)
- [Install ROS config, launch, urdf files](/CMakeLists.txt#L58-61): `INSTALL(DIRECTORY)` is recursive and preserves directory structure. You may want to copy additionnal directories (eg: `meshes`) depending on your package.

Caveats
---
You cannot run two ROS applications because the first one kills the second one and that leads to problems because the ROS core is sometimes not killed.

You should wait a few seconds for the application to shut-down before launching an other ROS desktop file.

